package client_remoteWS;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.WebSocketFrame;
import model.WebSocketFrame.Opcode;
import exception.BadValueException;

public class ReadDataFromRemoteWS implements Runnable {
	private volatile Socket clientSocket;
	private final InputStream serverInStr;
	private final OutputStream serverOutStr;
	private volatile Boolean connected;
	private static volatile Logger LOG;

	public ReadDataFromRemoteWS(final Socket clientSocketIn,
			final Logger logIn, final Boolean connected) throws IOException {

		/** check that necessary input values are not null */
		if (clientSocketIn != null && logIn != null) {
			clientSocket = clientSocketIn;
			serverInStr = clientSocket.getInputStream();
			serverOutStr = clientSocket.getOutputStream();
			LOG = logIn;
			this.connected = connected;
		} else
			throw new NullPointerException(
					"the logger or the client socket cannot be null");
	}

	/**
 *
 */
	@Override
	public void run() {

		try {
			getDataFromServer();
		} catch (final IOException e) {
			LOG.log(Level.SEVERE,
					" error in the server connection : " + e.getMessage());
			Thread.currentThread().interrupt();
		}
	}// end run

	/**
	 * @throws IOException
	 * @pre handshake was completed
	 */
	private void getDataFromServer() throws IOException {

		WebSocketFrame webFrameReceived, webSocketFrameToSend;
		/**
		 * reachable
		 */
		try {
			byte[] clientMaskKey = new byte[0];
			/** read from input stream until the client closes the connection */
			while (!clientSocket.isClosed() && isConnected()
					&& !Thread.currentThread().isInterrupted()) {

				/** generate unique masking key */
				clientMaskKey = generateNonce(4);
				if (clientMaskKey.length != 4)
					throw new IllegalArgumentException("Incorrect masking key");
				final boolean expectMask = false;

				// read here
				webFrameReceived = getFrame(expectMask);

				/** if received PING reply with PONG */
				if (webFrameReceived.getOpcode().getValue() == Opcode.PING
						.getValue()) {
					/** is fin, opcode, payload */
					webSocketFrameToSend = new WebSocketFrame(true,
							Opcode.PONG, webFrameReceived.getPayload());
					sendFrame(webSocketFrameToSend, clientMaskKey);

				}// end pong reply
				/** if received CLOSE_CONNECTION close connection */
				if (webFrameReceived.getOpcode().getValue() == Opcode.CLOSE_CONNECTION
						.getValue()) {

					/* close the connection */
					stopConnection();
					break;
				}
				/** We agreed not to support CONTINUATION_FRAME */

				/** if received TEXT_FRAME call onMessage */
				if (webFrameReceived.getOpcode().getValue() == Opcode.TEXT_FRAME
						.getValue()) {

					/** send server reply to the client */
					String serverMsg = new String(webFrameReceived.getPayload());
					if (serverMsg.contains("\"")) {
						// get the message from the game
						serverMsg = serverMsg.split("\"")[1];
						outputMssgToClient(serverMsg);
					}
				}
			}// end of socket input loop
		} catch (BadValueException | IOException | NullPointerException
				| NoSuchAlgorithmException e) {
			LOG.log(Level.SEVERE,
					" could not get data from the server: " + e.getMessage());
			stopConnectionOnError();
		}
	}// end of getting server data

	private void outputMssgToClient(final String msg) {

		System.out.println();
		System.out.println("+" + msg);
		System.out.print("-");
	}

	/**
	 * Generate numBytesToGen random bytes
	 * 
	 * @param numBytesToGen
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	private byte[] generateNonce(final int numBytesToGen)
			throws NoSuchAlgorithmException {

		byte[] randBytes = null;

		/** create random generator */
		final SecureRandom gener = SecureRandom.getInstance("SHA1PRNG");
		gener.setSeed((long) Math.random());
		/** get 16 random bits */
		randBytes = new byte[numBytesToGen];
		gener.nextBytes(randBytes);

		return randBytes;
	}

	protected void stopConnection() throws IOException {

		connected = false;
		/** stop the connection */
		if (!clientSocket.isClosed()) {
			try {
				clientSocket.close();
			} catch (final IOException e1) {
				LOG.log(Level.SEVERE,
						" could not close the server connection : "
								+ e1.getMessage());
			}

			if (!clientSocket.isClosed())
				throw new IOException("Could not close server socket.");
		}
		return;
	}

	protected void stopConnectionOnError() throws IOException {

		connected = false;
		/** stop the connection */
		if (!clientSocket.isClosed()) {
			try {
				clientSocket.close();

			} catch (final IOException e1) {
				LOG.log(Level.SEVERE,
						" could not close the server connection : "
								+ e1.getMessage());
			}

			if (!clientSocket.isClosed())
				throw new IOException("Could not close server socket.");
		}
		Thread.currentThread().interrupt();
		return;
	}

	private Boolean isConnected() {

		return connected;
	}

	/**
	 * Send the frame message
	 * 
	 * @param frameToSend
	 * @param clientMaskKeyIn
	 * @throws IOException
	 * @throws BadValueException
	 */
	private void sendFrame(final WebSocketFrame frameToSend,
			final byte[] clientMaskKeyIn) throws IOException, BadValueException {

		synchronized (serverOutStr) {
			if (!clientSocket.isClosed()) {
				frameToSend.encode(serverOutStr, clientMaskKeyIn);
				serverOutStr.flush();
			}
		}

	}// end send frame

	/**
	 * Read the frame message
	 * 
	 * @param expMask
	 * @return
	 * @throws IOException
	 * @throws BadValueException
	 */
	private WebSocketFrame getFrame(final boolean expMask) throws IOException,
			BadValueException, NullPointerException {

		WebSocketFrame resultFrame = null;
		synchronized (serverInStr) {
			if (!clientSocket.isClosed()) {
				resultFrame = new WebSocketFrame(serverInStr, expMask);
			} else {

			}
		}
		if (resultFrame == null)
			throw new NullPointerException();
		return resultFrame;
	}// end get frame

}// end class