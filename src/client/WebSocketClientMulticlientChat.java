package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.WebSocketFrame;
import model.WebSocketFrame.Opcode;

import org.apache.commons.codec.binary.Base64;

import exception.BadValueException;

public class WebSocketClientMulticlientChat {

    private volatile static Logger        LOG                                     = Logger.getLogger(WebSocketClientMulticlientChat.class
                                                                                          .getName());
    private final HashMap<String, String> serverResponseHeader                    = new HashMap<String, String>();
    private String                        userName;
    private final Socket                  clientSocket;
    /** server input stream */
    private final InputStream             serverInStr;
    private final OutputStream            serverOutStr;
    private final String                  requestedApp;

    /** shows if the client application has successfully connected to the server */
    private volatile Boolean              connected;
    private final String                  webSocketKey;
    private Thread                        readServerDataThread;
    private final Scanner                 clientInputStream                       = new Scanner(
                                                                                          System.in);
    /** GUUI used to calculate the key returned to client */
    private final static String           GUID                                    = new String(
                                                                                          "258EAFA5-E914-47DA-95CA-C5AB0DC85B11");

    /** default encoding */
    private final String                  encode                                  = "UTF-8";

    /** Handshake default values as specified in RFC 6455 */
    private final String                  HEADER_GET                              = "GET";
    private final String                  HEADER_UPGRADE_KEY                      = "upgrade";
    private final String                  HEADER_UPGRADE_VALUE                    = "upgrade";
    private final String                  HEADER_WEBSOCKET                        = "websocket";
    private final String                  HEADER_SEC_WEBSOCKET_ACCEPT             = "sec-websocket-accept";
    private final String                  HEADER_ACCEPTABLE_WEBSOCKET_VERSION     = "13";
    private final String                  HEADER_ACCEPTABLE_HTTP_PROTOCOL_VERSION = "HTTP/1.1";
    private final String                  HEADER_CONNECTION                       = "connection";
    private final String                  HEADER_DIEGEST_ALGORITHM                = "SHA-1";
    private final String                  SECURE_RANDOM_ALGORITHM                 = "SHA1PRNG";

    /** class for getting data from the server */
    private ReadDataFromServer            getData;

    /** wait for input stream thread to end in milliseconds */
    final int                             WAIT_FOR_COMPLETION                     = 1000;

    /**
     *
     * @param userNameIn
     * @param hostIn
     * @param portIn
     * @param requestedApp
     * @throws UnknownHostException
     * @throws IOException
     * @throws NullPointerException
     * @throws BadValueException
     * @throws NoSuchAlgorithmException
     */
    public WebSocketClientMulticlientChat(final String userNameIn,
            final String hostIn, final int portIn, final String requestedApp)
            throws UnknownHostException,
            IOException, NullPointerException, NoSuchAlgorithmException,
            IllegalArgumentException {

        /** check that necessary input values are not null */
        if (userNameIn != null && hostIn != null && portIn != 0
                && requestedApp != null) {

            userName = userNameIn;
            this.requestedApp = requestedApp;
            clientSocket = new Socket(hostIn, portIn);
            serverInStr = clientSocket.getInputStream();
            serverOutStr = clientSocket.getOutputStream();
            webSocketKey = getwebSocketKey();
        } else
            throw new NullPointerException();
    }

    /**
     * Connect to the server and process request and response
     *
     * @throws IOException
     * @throws InterruptedException
     * @throws NullPointerException
     * @throws NoSuchAlgorithmException
     * @throws BadValueException
     */
    public void init() throws IOException, InterruptedException,
            NullPointerException, NoSuchAlgorithmException, BadValueException {
        performHandShake();
        if (isConnected()) {
            getData = new ReadDataFromServer(clientSocket,
                    LOG, connected);
            readServerDataThread = new Thread(getData, "sendDataToServer");
            readServerDataThread.start();
            getUserRequest();

        } else {
            outputMssgToClient("Could not connect to the server.");
            Thread.currentThread().interrupt();
        }
    }// end init

    /**
     * Get data from the client
     *
     * @return
     * @throws IOException
     * @throws InterruptedException
     * @throws NoSuchAlgorithmException
     */
    private void getUserRequest() throws NoSuchAlgorithmException,
            InterruptedException, IOException {

        String clientInput = "";
        /** get data from the client and send it to the server */
        System.out.println("Enter exit to stop the chat.");

        while (!clientSocket.isClosed() && isConnected()
                && !Thread.currentThread().isInterrupted()) {

            /** get the data from the client */
            System.out.print("-");
            clientInput = clientInputStream.nextLine();
            if (clientInput.contains("exit")) {
                stopClient();
                break;
            }
            sendMsg(clientInput);
        }// end of socket output loop
        if(!clientSocket.isClosed()){
            stopClientOnErr("expected socket to be closed ");
        }
    }

    /**
     * Send data to Server
     *
     * @throws NoSuchAlgorithmException
     * @throws BadValueException
     * @throws InterruptedException
     * @throws IOException
     */
    public void sendMsg(final String clientInput)
            throws NoSuchAlgorithmException,
            InterruptedException, IOException {

        byte[] clientMaskKey = new byte[0];
        /** generate unique masking key */
        clientMaskKey = generateNonce(4);
        if (clientMaskKey.length != 4)
            throw new IllegalArgumentException(
                    "Incorrect masking key");

        if (clientInput == null) {
            stopClientOnErr("Problem reading client input");
            return; // exit loop and stop sending
        }
        /** send text frame to the server */
        // {"nickname":"guest","content":"Hello, WebSocket!"}
        final byte[] payload = ("{\"nickname\":\"" + userName
                + "\",\"content\":\"says " + clientInput + "\"}")
                .getBytes(encode);
        try {
            final WebSocketFrame webSocketFrameToSend = new WebSocketFrame(
                    true,
                    Opcode.TEXT_FRAME, payload);

            sendFrame(webSocketFrameToSend, clientMaskKey);
        } catch (final BadValueException e) {
            LOG.log(Level.FINE, "bad frame");
        }

    }

    /**
     * Send the frame message
     *
     * @param frameToSend
     * @param clientMaskKeyIn
     * @throws IOException
     * @throws BadValueException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    private void sendFrame(final WebSocketFrame frameToSend,
            final byte[] clientMaskKeyIn)
            throws IOException, BadValueException {

        synchronized (serverOutStr) {
            final byte[] dataFrame = frameToSend.encode(clientMaskKeyIn);
            System.out.println("Frame size " + dataFrame.length);
            frameToSend
                    .encode(serverOutStr, clientMaskKeyIn);
            serverOutStr.flush();
        }
    }// end send frame

    /**
     * Display message to the client
     *
     * @param msg
     */
    private void outputMssgToClient(final String msg) {

        System.out.println(msg);
    }

    /**
     * Check client connection status
     *
     * @return
     */
    public boolean isConnected() {

        return connected;
    }

    /**
     * Stop client
     *
     * @throws InterruptedException
     * @throws BadValueException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     *
     */
    public void stopClient() throws InterruptedException, IOException,
            NoSuchAlgorithmException {

        byte[] clientMaskKey = new byte[] {};
        /** generate unique masking key */
        clientMaskKey = generateNonce(4);
        if (clientMaskKey.length != 4)
            throw new IllegalArgumentException(
                    "Incorrect masking key");

        /** stop the application */
        try {

            final WebSocketFrame webSocketFrameToSend = new WebSocketFrame(
                    true,
                    Opcode.CLOSE_CONNECTION, new byte[]{});
            sendFrame(webSocketFrameToSend, clientMaskKey);
        } catch (final BadValueException e) {
            LOG.log(Level.SEVERE, "bad frame");
        }
        readServerDataThread.join();
        return;
    }

    /**
     * Stop client
     *
     * @param msg
     * @throws IOException
     */
    public void stopClientOnErr(final String msg) throws IOException {

        outputMssgToClient(msg);
        clientSocket.close();
        Thread.currentThread().interrupt();
        return;
    }

    /**
     * Helper method for opening handshake. Build the server response using
     * stored client request headers
     * {@link WebSocketConnectionHandler#serverResponseHeader} as specified
     * in RFC 6455.
     *
     * @throws IOException
     * @throws BadValueException
     * @throws NullPointerException
     * @throws NoSuchAlgorithmException
     */
    private void performHandShake() throws IOException,
            BadValueException, NullPointerException, NoSuchAlgorithmException {

        /** send an opening handshake */
        generateClientRequest();

        /** process server's handshake in response */
        try {
            parseServerReply();
        } catch (InterruptedException | ExecutionException exp) {
            LOG.log(Level.FINE,
                    " Problem with reading from the client channel "
                            + exp.toString());
        }

        /** check that the server replied with correct key */
        final String expectedServerKey = generateServerKey();

        /* check if the server reply is as specified in RFC 6455 */
        try {
            if (!serverResponseHeader.get(HEADER_UPGRADE_KEY).toLowerCase()
                    .equals(HEADER_WEBSOCKET)
                    || !serverResponseHeader.get(HEADER_CONNECTION)
                            .toLowerCase()
                            .contains(
                                    HEADER_UPGRADE_VALUE)
                    || !serverResponseHeader.get(HEADER_SEC_WEBSOCKET_ACCEPT)
                            .equals(
                                    expectedServerKey)) {
                LOG.log(Level.FINE, "The protocol RFC 6455 not followed");
                throw new IOException();
            }
            connected = true;

        } catch (final Exception e) {
            connected = false;
            LOG.log(Level.FINE, e.getMessage());
            throw new IOException();
        }

    }// end performOpeningHandShakeFromSocket

    /**
     * Generate client request
     *
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private void generateClientRequest() throws UnsupportedEncodingException,
            IOException, NoSuchAlgorithmException {

        /**
         * supply a /host/, /port/, /resource name/, and a /secure/ flag
         * list of /protocols/ and /extensions/
         */
        final String[] clientRequest = new String[7];
        /** prepare server reply */
        if (!clientSocket.isClosed()) {
            clientRequest[0] = HEADER_GET + " " + requestedApp + " "
                    + HEADER_ACCEPTABLE_HTTP_PROTOCOL_VERSION + "\r\n";
            clientRequest[1] = "Host: command.line.chat.client\r\n";
            clientRequest[2] = "Upgrade: websocket\r\n";
            clientRequest[3] = "Connection: " + "Upgrade\r\n";
            clientRequest[4] = "Sec-WebSocket-Key: " + webSocketKey + "\r\n";
            clientRequest[5] = "Sec-WebSocket-Version: "
                    + HEADER_ACCEPTABLE_WEBSOCKET_VERSION + "\r\n";
            /** end of request headers */
            clientRequest[6] = "\r\n";
        }

        /** send client request */
        for (int i = 0; i < clientRequest.length; i++) {
            if (!clientSocket.isClosed()) {
                synchronized (serverOutStr) {
                    serverOutStr.write(clientRequest[i].getBytes("UTF-8"));
                    serverOutStr.flush();
                }
            } else {
                stopClientOnErr(" connection terminated ");

            }

        }
    }

    /**
     * Helper function to read the client request and store request headers.
     *
     * @throws IOException
     * @throws ExecutionException
     * @throws InterruptedException
     * @throws TimeoutException
     */
    private void parseServerReply() throws IOException, InterruptedException,
            ExecutionException {

        if (!clientSocket.isClosed()) {

            String[] tempArr;

            /** use BufferedReader to read one line at a time */
            final BufferedReader readInputStream = new BufferedReader(
                    new InputStreamReader(serverInStr));

            String serverResponse;
            /** check if the request contains proper first line */
            if ((serverResponse = readInputStream.readLine()) != "") {
                if (!serverResponse
                        .equals(HEADER_ACCEPTABLE_HTTP_PROTOCOL_VERSION
                                + " 101 Switching Protocols")) {
                    LOG.log(Level.FINE,
                            " incorrect server sepnonse"
                                    + serverResponse);
                    throw new IOException("incorrect server sepnonse");
                }
            } else {
                LOG.log(Level.FINE, "The header format is incorrect "
                        + serverResponse);
                throw new IOException();
            }
            /** read the next line of the header */
            String requestLine = readInputStream.readLine();

            /* read server header until the empty line, which is the end of
             * the
             * header */
            while (!requestLine.equals("")
                    && !Thread.currentThread().isInterrupted()) {
                /** remove trailing spaces */
                requestLine = requestLine.replace(" ", "");
                /** split the header line into 2 */
                tempArr = requestLine.split(":", 2);

                if (tempArr.length == 2) {
                    /** store the header values into the map */
                    serverResponseHeader.put(tempArr[0].toLowerCase(),
                            tempArr[1]);
                } else {
                    LOG.log(Level.FINE, "The header format is incorrect "
                            + requestLine);
                    throw new IOException();
                }
                /** read next header line */
                requestLine = readInputStream.readLine();
            }// end while loop

        } else {
            stopClientOnErr(" connection terminated ");
        }
    }// end parseClientRequest

    /**
     * Nonce consisting of a randomly selected 16-byte value that has
     * been base64-encoded. Nonce randomly selected for each connection.
     *
     * @return client masking key
     * @throws NoSuchAlgorithmException
     *
     */
    private String getwebSocketKey() throws NoSuchAlgorithmException
    {

        /* generate 16 random bytes */
        final byte[] nonce = generateNonce(16);
        /** encode the randomly generated key with base64 */
        final String serverReplyKey = new String(Base64.encodeBase64(nonce));
        return serverReplyKey;
    }

    /**
     * Generate numBytesToGen random bytes
     *
     * @param numBytesToGen
     * @return
     * @throws NoSuchAlgorithmException
     */
    private byte[] generateNonce(final int numBytesToGen)
            throws NoSuchAlgorithmException {

        byte[] randBytes = null;

        /** create random generator */
        final SecureRandom gener = SecureRandom
                .getInstance(SECURE_RANDOM_ALGORITHM);
        gener.setSeed((long) Math.random());
        /** get 16 random bits */
        randBytes = new byte[numBytesToGen];
        gener.nextBytes(randBytes);

        return randBytes;
    }

    /**
     * Get server key based on client key with
     * GUID, digest it with SHA-1 and encode
     * it in base64 as defined in RFC 6455.
     *
     * @param clientKey
     * @return String server accept key
     * @throws NoSuchAlgorithmException
     * if SHA-1 is not available
     */
    private String generateServerKey() throws NoSuchAlgorithmException {

        /** add required magic GUID as stated in RFC 6455 */
        final String keyReply = webSocketKey + GUID;
        /** get SHA-1 algorithm */
        final MessageDigest md = MessageDigest
                .getInstance(HEADER_DIEGEST_ALGORITHM);
        /** load the key into the SHA-1 */
        md.update(keyReply.getBytes(), 0, keyReply.length());
        /** produce SHA-1 digest message */
        final byte[] hashedKey = md.digest();
        /** encode the hashedKey with base64 */
        final String serverReplyKey = new String(Base64.encodeBase64(hashedKey));
        return serverReplyKey;
    }

    /**
     * @param args
     * @throws IOException
     * @throws BadValueException
     * @throws InterruptedException
     * @throws IllegalArgumentException
     * @throws NoSuchAlgorithmException
     * @throws NullPointerException
     */
    public static void main(final String args[]) throws InterruptedException,
            BadValueException, IOException, NullPointerException,
            NoSuchAlgorithmException, IllegalArgumentException {

        WebSocketClientMulticlientChat client = null;
        try{
        final String[] userRequest = getConnectionParams(args);
        int port = 0;
        port = Integer.parseInt(userRequest[2]);

        /** pass user name, host, port, requested app */
        client = new WebSocketClientMulticlientChat(
                userRequest[0], userRequest[1], port, userRequest[3]);
        client.init();
    }catch(Exception e){
    	LOG.log(Level.SEVERE, "problem with socket connection");
    }
    }// end main

    /**
     * Get the user request
     *
     * @param args
     * @return
     */
    protected static String[] getConnectionParams(final String[] args) {

        final String[] result = new String[4];
        String userName, serverWSaddr;
        /** get from command line args */
        if (args.length != 2) {
            System.out.println("Usage <user name> <valid ws address>");
        }
        userName = args[0];
        serverWSaddr = args[1];

        /** "ws:" "//" authority path-abempty [ "?" query ] */
        final Pattern wsPattern = Pattern
                .compile("(ws://)?(wss://)?[a-zA-Z_0-9\\\\-\\\\.]+\\:[0-9]+\\/[a-zA-Z_0-9\\\\-\\\\.]*");

        final Matcher checkTheMatch = wsPattern.matcher(serverWSaddr.trim());
        if (!checkTheMatch.matches()) {
            System.out
                    .println("please provide ws uri in correct format as stated in RFC 6455.");
            LOG.log(Level.SEVERE, "incorrectly formatted ws URI");
        }

        final String[] request = serverWSaddr.trim().split(":");

        /** for now only ws, wss not supported here */
        final String protocol = request[0].trim();
        if (!protocol.equals("ws"))
            throw new IllegalArgumentException(
                    "The protocol " + protocol + " is not supported.");

        final String host = request[1].replace("//", "").trim();
        final String[] portPath = request[2].trim().split("/");
        final String port = portPath[0];
        String requestedApp = "";

        if (portPath.length > 1) {
            for (int i = 1; i < portPath.length; i++) {
                requestedApp = requestedApp + "/" + portPath[i];
            }
        } else {
            requestedApp = "/";
        }
        result[0] = userName;
        result[1] = host;
        result[2] = port;
        result[3] = requestedApp;

        return result;
    }// end of get conn param

}